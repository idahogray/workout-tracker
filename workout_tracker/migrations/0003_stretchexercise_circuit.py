# Generated by Django 4.0.4 on 2022-04-15 10:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workout_tracker', '0002_workout_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='stretchexercise',
            name='circuit',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
