# Generated by Django 4.0.4 on 2022-05-03 22:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workout_tracker', '0008_remove_workout_weight_exercise_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bodyweightexercise',
            options={'ordering': ['exercise__name']},
        ),
        migrations.AlterModelOptions(
            name='exercise',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='stretchexercise',
            options={'ordering': ['exercise__name']},
        ),
        migrations.AlterModelOptions(
            name='weightexercise',
            options={'ordering': ['exercise__name']},
        ),
        migrations.AddField(
            model_name='weightexercise',
            name='difficulty',
            field=models.CharField(choices=[('Easy', 'Easy'), ('Good', 'Good'), ('Hard', 'Hard')], default='EASY', max_length=200),
            preserve_default=False,
        ),
    ]
