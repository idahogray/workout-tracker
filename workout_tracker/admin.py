from django.contrib import admin
from model_clone import CloneModelAdmin

from .models import (BodyWeightExercise, CardioExercise, Exercise, Person,
                     StretchExercise, Weight, WeightExercise, Workout)


class WeightExerciseInline(admin.TabularInline):
    model = WeightExercise


class CardioExerciseInline(admin.TabularInline):
    model = CardioExercise


class WeightExerciseAdmin(admin.ModelAdmin):
    list_filter = ('workout__person', )


class WorkoutAdmin(CloneModelAdmin):
    inlines = [
        CardioExerciseInline,
        WeightExerciseInline,
    ]


# Register your models here.
admin.site.register(BodyWeightExercise)
admin.site.register(CardioExercise)
admin.site.register(Exercise)
admin.site.register(Person)
admin.site.register(StretchExercise)
admin.site.register(Weight)
admin.site.register(WeightExercise, WeightExerciseAdmin)
admin.site.register(Workout, WorkoutAdmin)
