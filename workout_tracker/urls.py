from django.urls import path

from .views import WorkoutDetailView, WorkoutListView

app_name = 'workout_tracker'
urlpatterns = [
    path('workouts/', WorkoutListView.as_view(), name="workout-list"),
    path('workouts/<int:pk>/', WorkoutDetailView.as_view(), name="workout-detail"),
]