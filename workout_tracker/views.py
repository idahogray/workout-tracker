from django.views.generic import DetailView, ListView

from .models import Workout


class WorkoutListView(ListView):
    model = Workout
    context_object_name = "workouts"


class WorkoutDetailView(DetailView):
    model = Workout