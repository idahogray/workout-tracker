from collections import defaultdict
from enum import Enum, auto, unique
from itertools import chain

from django.db import models
from django.urls import reverse
from model_clone import CloneMixin


class WorkoutPhaseChoices(models.TextChoices):
    WARMUP = "Warmup"
    WORKOUT = "Workout"
    COOLDOWN = "Cooldown"


class WorkoutDifficultyChoices(models.TextChoices):
    EASY = "Easy"
    GOOD = "Good"
    HARD = "Hard"


class Exercise(models.Model):
    name = models.CharField(max_length=200)
    description_link = models.URLField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class StretchExercise(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    sets = models.IntegerField()
    time = models.DurationField(blank=True, null=True)
    phase = models.CharField(choices=WorkoutPhaseChoices.choices, max_length=200)
    circuit = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.exercise} - {self.sets} sets of {self.time} (Circuit {self.circuit})" if self.circuit > 0 else f"{self.exercise} - {self.sets} sets of {self.time}"

    class Meta:
        ordering = ['exercise__name']


class BodyWeightExercise(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    sets = models.IntegerField()
    reps = models.IntegerField(blank=True, null=True)
    time = models.DurationField(blank=True, null=True)
    circuit = models.IntegerField(default=0)
    phase = models.CharField(choices=WorkoutPhaseChoices.choices, max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.exercise} - {self.sets} sets of {self.reps if self.reps else self.time} (Circuit {self.circuit})" if self.circuit > 0 else f"{self.exercise} - {self.sets} sets of {self.reps if self.reps else self.time}"

    class Meta:
        ordering = ['exercise__name']


class Person(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    canonical_name = models.CharField(max_length=200)

    def __str__(self):
        return self.canonical_name


class Workout(CloneMixin, models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    description = models.TextField(max_length=200)
    body_weight_exercise = models.ManyToManyField(BodyWeightExercise)
    stretch_exercise = models.ManyToManyField(StretchExercise)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    _clone_m2m_fields = ['body_weight_exercise', 'stretch_exercise']
    _clone_m2o_or_o2m_fields = ["cardioexercise_set", "weightexercise_set"]

    def __str__(self):
        return f"{self.person} - {self.start_time} {self.description}"

    def get_absolute_url(self):
        return reverse('workout_tracker:workout-detail', kwargs={'pk': self.pk})

    class Meta:
        ordering = ['-start_time']

    @property
    def cardio(self):
        self.cardio_exercise.filter(phase=WorkoutPhaseChoices.WARMUP),


    @property
    def warmups(self):
        return (
            self.cardioexercise_set.filter(phase=WorkoutPhaseChoices.WARMUP),
            self.body_weight_exercise.filter(phase=WorkoutPhaseChoices.WARMUP),
            self.weightexercise_set.filter(phase=WorkoutPhaseChoices.WARMUP),
            self.stretch_exercise.filter(phase=WorkoutPhaseChoices.WARMUP),
        )

    @property
    def workout(self):
        return (
            self.body_weight_exercise.filter(phase=WorkoutPhaseChoices.WORKOUT, circuit=0),
            self.weightexercise_set.filter(phase=WorkoutPhaseChoices.WORKOUT, circuit=0),
            self.stretch_exercise.filter(phase=WorkoutPhaseChoices.WORKOUT, circuit=0),
            self.cardioexercise_set.filter(phase=WorkoutPhaseChoices.WORKOUT),
        )

    @property
    def workout_circuit(self):
        circuits = defaultdict(list)
        query_sets = (
            self.body_weight_exercise.filter(phase=WorkoutPhaseChoices.WORKOUT, circuit__gt=0),
            self.weightexercise_set.filter(phase=WorkoutPhaseChoices.WORKOUT, circuit__gt=0),
            self.stretch_exercise.filter(phase=WorkoutPhaseChoices.WORKOUT, circuit__gt=0),
            self.cardioexercise_set.filter(phase=WorkoutPhaseChoices.WORKOUT),
        )
        for exercise in chain(*query_sets):
            circuits[exercise.circuit].append(exercise)
        circuits.default_factory = None
        print(sorted(circuits.items()))
        return sorted(circuits.items())


    @property
    def cooldown(self):
        return (
            self.body_weight_exercise.filter(phase=WorkoutPhaseChoices.COOLDOWN),
            self.weightexercise_set.filter(phase=WorkoutPhaseChoices.COOLDOWN),
            self.stretch_exercise.filter(phase=WorkoutPhaseChoices.COOLDOWN),
            self.cardioexercise_set.filter(phase=WorkoutPhaseChoices.COOLDOWN),
        )


class CardioExercise(models.Model):

    class Intensity(models.IntegerChoices):
        LIGHT = auto()
        MODERATE = auto()
        VIGOROUS = auto()
        HEART_RATE = auto()

    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    time = models.DurationField()
    distance_miles = models.FloatField()
    phase = models.CharField(choices=WorkoutPhaseChoices.choices, max_length=200)
    intensity = models.IntegerField(choices=Intensity.choices)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    workout = models.ForeignKey(Workout, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.exercise}: {self.distance_miles} miles in {self.time}"


class WeightExercise(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    sets = models.IntegerField()
    reps = models.IntegerField()
    weight = models.FloatField(blank=True, null=True)
    phase = models.CharField(choices=WorkoutPhaseChoices.choices, max_length=200)
    circuit = models.IntegerField(default=0)
    difficulty = models.CharField(choices=WorkoutDifficultyChoices.choices, max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    workout = models.ForeignKey(Workout, on_delete=models.CASCADE)

    def __str__(self):
        return (
            f"{self.exercise} - {self.sets} sets of {self.reps} "
            f"@{self.weight}lbs (Circuit {self.circuit}) - {self.difficulty} "
            f" on {self.workout.start_time.date()}"
        ) if self.circuit > 0 else (
            f"{self.exercise} - {self.sets} sets of {self.reps} "
            f"@{self.weight}lbs - {self.difficulty} "
            f" on {self.workout.start_time.date()}"
        )

    class Meta:
        ordering = ['exercise__name']



class Weight(models.Model):
    weight = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
